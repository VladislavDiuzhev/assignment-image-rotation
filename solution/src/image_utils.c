//
// Created by vladyslav on 10/4/22.
//

#include "image_utils.h"
#include <malloc.h>

void image_initialize_basic_params(uint64_t width, uint64_t height, struct image *img) {
    img->width = width;
    img->height = height;
    img->data = malloc(img->height * img->width * sizeof(struct pixel));
}

void image_free_pixel_data(struct image *img) {
    if (img != NULL) {
        free(img->data);
    }
}

void image_copy_free_old(struct image *from, struct image *to) {
    image_free_pixel_data(to);
    to->data = from->data;
    to->height = from->height;
    to->width = from->width;
}

size_t get_pixel_index_by_cords(const struct image *img, uint64_t row, uint64_t col) {
    return img->width * row + col;
}

struct pixel *get_pixel_by_cords(const struct image *img, uint64_t row, uint64_t col) {
    return img->data + get_pixel_index_by_cords(img, row, col);
}

void set_pixel_by_cords(struct image *img, uint64_t row, uint64_t col, struct pixel px) {
    *get_pixel_by_cords(img, row, col) = px;
}

void set_pixel_by_index(struct image *img, size_t ind, struct pixel px) {
    img->data[ind] = px;
}
