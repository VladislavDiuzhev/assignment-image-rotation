//
// Created by vladyslav on 10/2/22.
//

#include "bmp_utils.h"
#include "image_utils.h"
#include <malloc.h>
#include <stdbool.h>


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


static uint8_t bmp_calculate_padding(uint64_t imageWidth) {
    return (4 - ((3 * imageWidth) % 4)) % 4;
}

static uint32_t bmp_calculate_file_size(uint64_t imageWidth, uint64_t imageHeight) {
    return imageHeight * imageWidth * sizeof(struct pixel) + imageHeight * bmp_calculate_padding(imageWidth);
}

static bool check_header(struct bmp_header *bmpHeader) {
    return bmpHeader->bfType == BM_IDENTIFIER
           && bmpHeader->biSize == INFO_HEADER_SIZE
           && bmpHeader->biPlanes == PLANES_NUM
           && bmpHeader->bOffBits >= sizeof(struct bmp_header)
           && bmpHeader->biBitCount == sizeof(struct pixel) * 8;
}

static enum bmp_read_status read_header(struct bmp_header *bmpHeader, FILE *stream) {
    fread(bmpHeader, sizeof(struct bmp_header), 1, stream);
    if (ferror(stream) == 0 && check_header(bmpHeader)) {
        return READ_OK;
    } else {
        return READ_INVALID_HEADER;
    }
}

static struct bmp_header bmp_generate_header(struct image const *img) {
    struct bmp_header header = {0};
    header.bfType = BM_IDENTIFIER; // "BM" - file type
    header.biSize = INFO_HEADER_SIZE; // Size of bitmap info header
    header.biPlanes = PLANES_NUM; // Planes number
    header.bfileSize = bmp_calculate_file_size(img->width, img->height); // Size of image
    header.biWidth = img->width; // Image width
    header.biHeight = img->height; // Image height
    header.biBitCount = sizeof(struct pixel) * 8; // Bits per pixel
    header.bOffBits = sizeof(struct bmp_header); // Bits offset
    return header;
}

enum bmp_read_status from_bmp(FILE *in, struct image *img) {
    // Check if the input stream is available
    if (in == NULL) {
        return READ_INVALID_PATH;
    }

    // Reading bmp header
    struct bmp_header bmpHeader;
    if (read_header(&bmpHeader, in) != READ_OK) { // Check if reading was successful
        return READ_INVALID_HEADER;
    }

    if (fseek(in, bmpHeader.bOffBits, SEEK_SET) != 0) { // Setting position to read pixels
        return READ_INVALID_SIGNATURE;
    }

    struct image pot_image;

    // Create initial structure of image
    image_initialize_basic_params(bmpHeader.biWidth, bmpHeader.biHeight, &pot_image);
    image_initialize_basic_params(bmpHeader.biWidth, bmpHeader.biHeight, img);
    if (!pot_image.data) { // Check if memory allocation was successful
        return READ_INVALID_SIGNATURE;
    }

    const uint8_t paddingNum = bmp_calculate_padding(bmpHeader.biWidth); // Get padding number
    
    for (uint32_t i = 0; i < bmpHeader.biHeight; i++) {
        fread(pot_image.data + i * pot_image.width, sizeof(struct pixel), pot_image.width, in);
        if (ferror(in) != 0) { // Check if an error occurred while reading
            image_free_pixel_data(&pot_image);
            return READ_INVALID_BITS;
        }
        if (fseek(in, paddingNum, SEEK_CUR) != 0) {
            image_free_pixel_data(&pot_image);
            return READ_INVALID_SIGNATURE;
        }
    }
    image_copy_free_old(&pot_image,img);
    return READ_OK;
}


enum bmp_write_status to_bmp(FILE *out, struct image const *img) {
    // Check if the output stream is available
    if (!out) {
        return WRITE_ERROR;
    }

    struct bmp_header bmpHeader = bmp_generate_header(img); // Creating header
    fwrite(&bmpHeader, sizeof(struct bmp_header), 1, out);
    if (ferror(out) != 0) {
        return WRITE_ERROR;
    }

    const char garb[] = {0, 0, 0}; // Garbage bytes for padding
    const uint8_t paddingNum = bmp_calculate_padding(img->width); // Get padding number

    for (uint64_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fwrite(garb, 1, paddingNum, out);
        if (ferror(out) != 0) { // Check if an error occurred while writing
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
