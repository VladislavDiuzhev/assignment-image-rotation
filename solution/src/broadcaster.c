//
// Created by vladyslav on 10/7/22.
//

#include "broadcaster.h"
#include "transform_rotate90.h"

const char *const opening_messages[] = {
        [OPEN_OK] = "File opened successfully!\n",
        [OPEN_ERROR_FILE_NOT_FOUND] = "File not found error!\n"
};

const char *const closing_messages[] = {
        [CLOSE_OK] = "File closed successfully!\n",
        [CLOSE_ERROR] = "An error occurred while closing!\n"
};

const char *const reading_messages[] = {
        [READ_OK] = "Reading completed successfully!\n",
        [READ_INVALID_SIGNATURE] = "Invalid signature error occurred while reading!\n",
        [READ_INVALID_BITS] = "Invalid bits error occurred while reading!\n",
        [READ_INVALID_HEADER] = "Invalid header error occurred while reading!\n",
        [READ_INVALID_PATH] = "Invalid path error occurred while reading!\n",
};

const char *const writing_messages[] = {
        [WRITE_OK] = "Writing completed successfully!\n",
        [WRITE_ERROR] = "An error occurred wile writing!\n",
};

bool opening_wrapper(char const *filename, char const *mode, FILE **f) {
    enum open_status result = open_file(filename, mode, f);
    fprintf(stderr, "<%s>: %s", filename, opening_messages[result]);
    if (result != OPEN_OK) {
        fprintf(stdout, "Unable to open file (%s).\n", filename);
    }
    return result == OPEN_OK;
}

bool closing_wrapper(char const *filename, FILE **f) {
    enum close_status result = close_file(f);
    fprintf(stderr, "<%s>: %s", filename, closing_messages[result]);
    if (result != CLOSE_OK) {
        fprintf(stdout, "Unable to close file (%s).\n", filename);
    }
    return result == CLOSE_OK;
}

bool reading_wrapper(char const *filename, FILE *in, struct image *img) {
    enum bmp_read_status result = from_bmp(in, img);
    fprintf(stderr, "<%s>: %s", filename, reading_messages[result]);
    if (result != READ_OK) {
        fprintf(stdout, "Unable to read data from file (%s).\n", filename);
    }
    return result == READ_OK;
}

bool writing_wrapper(char const *filename, FILE *out, struct image *img) {
    enum bmp_write_status result = to_bmp(out, img);
    fprintf(stderr, "<%s>: %s", filename, writing_messages[result]);
    if (result != WRITE_OK) {
        fprintf(stdout, "Unable to write data to file (%s).\n", filename);
    }
    return result == WRITE_OK;
}

bool rotation_wrapper(struct image *rot, struct image const src) {
    bool result;
    *rot = transform_rotate_90_counter(src, &result);
    if (result) {
        fprintf(stderr, "<program>: Image rotated successfully!\n");
    } else {
        fprintf(stderr, "<program>: An error occurred while rotating image!\n");
        fprintf(stdout, "Unable to transform_rotate_90_counter image.\n");
    }
    return result;
}
