#include "transform_rotate90.h"
#include "broadcaster.h"
#include "file_utils.h"
#include <stdio.h>


int main(int argc, char **argv) {
    // Parsing arguments
    if (argc != 3) {
        printf("Invalid arguments! 2 required (<input_file_path>, <output_file_path>).\n");
        return 1;
    }
    const char *input_path = argv[1];
    const char *output_path = argv[2];

    printf("Input file: %s, Output file: %s\n", input_path, output_path);

    // Opening input file stream
    FILE *file_in;
    if (!opening_wrapper(input_path, "rb", &file_in)) {
        return 1;
    }
    // Reading image
    struct image img;
    if (!reading_wrapper(input_path, file_in, &img)) {
        return 1;
    }
    // Closing input file stream
    if (!closing_wrapper(input_path, &file_in)) {
        return 1;
    }

    // Rotating
    struct image new_img;
    if (!rotation_wrapper(&new_img, img)) {
        return 1;
    }

    // Free memory used to store input file
    image_free_pixel_data(&img);

    // Open output file stream
    FILE *file_out;
    if (!opening_wrapper(output_path, "wb", &file_out)) {
        return 1;
    }
    // Writing rotated image to output file
    if (!writing_wrapper(output_path, file_out, &new_img)) {
        return 1;
    }
    // Free memory used to store output file
    image_free_pixel_data(&new_img);
    // Closing output file stream
    if (!closing_wrapper(output_path, &file_out)) {
        return 1;
    }

    printf("Program finished!\n");
    return 0;
}
