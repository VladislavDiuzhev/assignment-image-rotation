//
// Created by vladyslav on 10/2/22.
//

#include "transform_rotate90.h"

struct image transform_rotate_90_counter(struct image const source, bool* res) {
    *res = true;
    // Creating new image
    struct image new_img;
    image_initialize_basic_params(source.height, source.width, &new_img);

    // Rotating
    for (uint32_t i = 0; i < source.height; i++) {
        for (uint32_t j = 0; j < source.width; j++) {
            size_t new_ind = j * source.height + (source.height - 1 - i);
            set_pixel_by_index(&new_img,new_ind,*get_pixel_by_cords(&source,i,j));
        }
    }
    return new_img;
}
