//
// Created by vladyslav on 10/7/22.
//

#include "file_utils.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>


enum open_status open_file(char const *filename, char const *mode, FILE **f) {
    if (strcmp(mode, "wb") == 0 || access(filename, F_OK) == 0) {
        *f = fopen(filename, mode);
        return OPEN_OK;
    }
    return OPEN_ERROR_FILE_NOT_FOUND;
}

enum close_status close_file(FILE **f) {
    if (f) {
        fclose(*f);
        return CLOSE_OK;
    }
    return CLOSE_ERROR;
}
