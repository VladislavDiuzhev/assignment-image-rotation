//
// Created by vladyslav on 10/2/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_UTILS_H

#define BM_IDENTIFIER 0x4D42
#define PLANES_NUM 1
#define INFO_HEADER_SIZE 40

#include "image_utils.h"
#include "stdio.h"
#include "stdint.h"


enum bmp_read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PATH,
};

enum bmp_read_status from_bmp(FILE *in, struct image *img);

enum bmp_write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum bmp_write_status to_bmp(FILE *out, struct image const *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_UTILS_H
