//
// Created by vladyslav on 10/7/22.
//

#include <bits/types/FILE.h>

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR_FILE_NOT_FOUND,
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum open_status open_file(char const *filename, char const *mode, FILE **f);

enum close_status close_file(FILE **f);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
