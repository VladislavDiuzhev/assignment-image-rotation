//
// Created by vladyslav on 10/2/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTILS_H

#include <stddef.h>
#include "stdint.h"

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

void image_initialize_basic_params(uint64_t width, uint64_t height, struct image *img);

void image_free_pixel_data(struct image *img);

void image_copy_free_old(struct image *from, struct image *to);

size_t get_pixel_index_by_cords(const struct image *img, uint64_t row, uint64_t col);

struct pixel *get_pixel_by_cords(const struct image *img, uint64_t row, uint64_t col);

void set_pixel_by_cords(struct image *img, uint64_t row, uint64_t col, struct pixel px);

void set_pixel_by_index(struct image *img, size_t ind, struct pixel px);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_UTILS_H
