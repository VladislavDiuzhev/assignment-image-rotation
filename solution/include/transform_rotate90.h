//
// Created by vladyslav on 10/2/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_ROTATE90_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_ROTATE90_H

#include <stdbool.h>
#include "image_utils.h"

/* Rotation 90 degrees counterclockwise */
struct image transform_rotate_90_counter(struct image source, bool *res);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_ROTATE90_H
