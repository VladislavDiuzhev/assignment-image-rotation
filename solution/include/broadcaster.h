//
// Created by vladyslav on 10/7/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BROADCASTER_H
#define ASSIGNMENT_IMAGE_ROTATION_BROADCASTER_H

#include "file_utils.h"
#include "bmp_utils.h"
#include <stdbool.h>

bool opening_wrapper(char const *filename, char const *mode, FILE **f);

bool closing_wrapper(char const *filename, FILE **f);

bool reading_wrapper(char const *filename, FILE *in, struct image *img);

bool writing_wrapper(char const *filename, FILE *out, struct image *img);

bool rotation_wrapper(struct image* rot, struct image src);

#endif //ASSIGNMENT_IMAGE_ROTATION_BROADCASTER_H
